from unittest import TestCase

__author__ = 'servio pantoja'

from Calculadora import Calculadora

class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(""), 0, "Cadena vacia")

    def test_sumar_una_cadena(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "Facil al sumar un numero")

    def test_sumar_dos_numero_diferentes(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "Fallo al sumer dos numeros diferentes")
        self.assertEqual(Calculadora().sumar("2"), 2, "Fallo al sumer dos numeros diferentes")

    def test_sumar_cadena_con_dos_numeros(self):
        self.assertEqual(Calculadora().sumar("1,2"), 3, "Fallo al sumer dos cadena con dos numeros")


    def test_sumar_cadena_con_multiples_numeros(self):
        self.assertEqual(Calculadora().sumar("1,1,1,1,1"), 5, "Fallo al sumar cadenas con multiples numeros")

    def test_sumar_cadena_con_numeros_y_multiples_separadores(self):
        self.assertEqual(Calculadora().sumar("1,1&1:1&1"), 5, "Fallo al sumar cadenas con multiples numeros")
